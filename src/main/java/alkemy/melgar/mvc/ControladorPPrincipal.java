package alkemy.melgar.mvc;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
public class ControladorPPrincipal {
	
	@RequestMapping ("/muestraPantallaPrincipal")//Url del formulario jsp
	public String muestraPantallaPrincipal() {
		
		return "pantallaPrincipal"; //seria el formulario
	}
	
	@RequestMapping ("/logInAdministrador")
	public String logInAdministrador () { // url del formulario procesado
		
		return "logInAdmin"; //formulario rellenado por el usuario
	}
	
	@RequestMapping ("/chequeoUsuario")
	public String chequeoUsuario (HttpServletRequest request, Model model) { //Probando validaion de usuario
		
		String nombreAdmin = request.getParameter("usuarioAdministrador");
		
		model.addAttribute ("usuarioAdmin", nombreAdmin);
		return "logInAdmin";
			
		}
	@RequestMapping("/logInAlumno")
	public String LogInAlumno () {
		
		return"logInAlumno";
		
	}
	
	@RequestMapping("contrasenaAlumno")
	public String contraseñaAlumno () {
		return "contraAlumno";
	}
	
	@RequestMapping("listaMaterias")
	public String listaMaterias ( ) {
		return "listaMaterias";
	}
	
	@RequestMapping ("confirmacionInscripcion")
	public String confirmacionInscripcion () {
		return "confirmacionInscripcion";
	}
	
	@RequestMapping ("bienvenidoAdmin")
	public String bienvenidoAdmin ( ) {
		return "bienvenidoAdmin";
	}
	}
	


