package alkemy.melgar.mvc;

public class Auxiliar {
	
	private String nombre;
	private String Apellido;
	private int dni;
	private int legajo;
	public String getNombre() {
		return nombre;
	}
	public String getApellido() {
		return Apellido;
	}
	public int getDni() {
		return dni;
	}
	public int getLegajo() {
		return legajo;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public void setApellido(String apellido) {
		Apellido = apellido;
	}
	public void setDni(int dni) {
		this.dni = dni;
	}
	public void setLegajo(int legajo) {
		this.legajo = legajo;
	}
	
	
}
